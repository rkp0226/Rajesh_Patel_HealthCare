package com.healthcare.insurance;

public class Insurance {

	Premium premium;

	public void setPremium(Premium premium) {
		this.premium = premium;
	}
	
	public int getPremiumAmount(Client client){
		return premium.calculatePremium(client);
	}
}
