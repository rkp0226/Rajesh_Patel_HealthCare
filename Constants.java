package com.healthcare.insurance;

public interface Constants {

	static final String MALE = "Male";
	static final String FEMALE = "Female";
	static final String OTHERS = "Other";
	
}
