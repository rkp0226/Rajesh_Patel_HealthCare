package com.healthcare.insurance;

public class HealthInsurancePremium implements Premium{
	
	private static final int BASE_PREMIUM = 5000;
	
	
	public HealthInsurancePremium() {}
	
	@Override
	public int calculatePremium(Client client) {
		
		int calcPremium = 0;
		
		if(client.getAge() < 18) {
			calcPremium = BASE_PREMIUM;
			return calcPremium;
		}
		if(client.getAge() >= 18 && client.getAge() <= 25 || 
				client.getAge() >= 25 && client.getAge() <= 30 || 
				client.getAge() >= 30 && client.getAge() <= 35 ||
				client.getAge() >= 35 && client.getAge() <= 40) {
			calcPremium = BASE_PREMIUM + (BASE_PREMIUM*10)/100;
		}else if(client.getAge() >= 40) {
			calcPremium += (BASE_PREMIUM*20)/100;
		}
		
		/*if(client.getGender().equals(Constants.MALE)) {
			calcPremium += (BASE_PREMIUM*2)/100;
		}
		
		if(client.isHavingHypertension() || client.isHavingBloodPressure() || 
				client.isHavingBloodSugar() || client.isHavingOverweight()) {
			
			calcPremium += (BASE_PREMIUM*1)/100;
		}
		
		if(client.isDoDailyExcercise()) {
			
			//Reduce premium
			calcPremium -= (BASE_PREMIUM*3)/100;
		}
		
		if(client.isSmoking() || client.isAlcholic() || client.isTakingDrugs()) {
			
			//Increase Premium
			calcPremium += (BASE_PREMIUM*3)/100;
		}*/
		
		return calcPremium;
	}

}
