package com.healthcare.insurance;

public interface Premium {

	public int calculatePremium(Client client);
}
