package com.healthcare.insurance.test;

import org.junit.Assert;
import org.junit.Test;

import com.healthcare.insurance.Client;
import com.healthcare.insurance.HealthInsurancePremium;
import com.healthcare.insurance.Insurance;


public class HealthInsurancePremiumTest {
	
	@Test
	public void getPremiumAmount(){
		
		Client client = new Client("Norman Gomes", "Male", 34, false, false, false, true, false, true, true, false);
		Insurance insurance = new Insurance();
		insurance.setPremium(new HealthInsurancePremium());
		Assert.assertEquals(5650, insurance.getPremiumAmount(client));
		
		
		System.out.println("Insurance Premium :" + insurance.getPremiumAmount(client));
	}

}
